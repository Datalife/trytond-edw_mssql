# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from .instance import Instance


def register():
    Pool.register(
        Instance,
        module='edw_mssql', type_='model')
