# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields
from .connector import BackendConnectorMSsql

__all__ = ['Instance']

MS_URL = "mssql://ipServer:1433/database"


class Instance(metaclass=PoolMeta):
    __name__ = 'edw.instance'

    @classmethod
    def __setup__(cls):
        super(Instance, cls).__setup__()
        sql_type = ('mssql', 'MS SQL Server')
        cls.type_.selection.append(sql_type)

    @fields.depends('uri')
    def on_change_type_(self):
        if not self.uri:
            self.uri = MS_URL

    def get_app_connector(self):
        if self.type_ == 'mssql':
            return BackendConnectorMSsql(self.uri,
                username=self.user,
                password=self.pwd)
        return super(Instance, self).get_app_connector()
